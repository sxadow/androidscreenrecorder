package com.example.capture;

import java.io.DataOutputStream;

public class CaptureUtil {

	static
	{
//		android.os.Debug.waitForDebugger();
		System.loadLibrary("capture");
	}
	
	/**
	 * @param fileName
	 */
	public static native void capture(String fileName);
	
	/**
	 * root
	 * 
	 * @param command
	 * @return
	 */
	public static boolean upgradeRootPermission(String pkgCodePath) {
		Process process = null;
		DataOutputStream os = null;
		try {
			String cmd = "chmod 777 " + pkgCodePath;
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes(cmd + "\n");
			os.writeBytes("mkdir /sdcard/.Capture" + "\n");
			os.writeBytes("rm /sdcard/out3.mp4" + "\n");
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
		} catch (Exception e) {
			return false;
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				process.destroy();
			} catch (Exception e) {
			}
		}
		return true;
	}
	
	
}
