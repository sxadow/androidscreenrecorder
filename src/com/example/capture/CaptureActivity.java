package com.example.capture;


import com.example.capture.R;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import android.os.Bundle;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class CaptureActivity extends Activity {

	private Button btnStartRecord;
	private Button btnStop;
	private ProgressBar pbar;
	private Intent serviceIntent;
	private boolean isFirst = true;
	private static final String TAG = CaptureActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "on create");
		super.onCreate(savedInstanceState);
		initComponents();
		if (((MyApplication) getApplication()).isRecording) {
			btnStop.setEnabled(true);
			btnStartRecord.setEnabled(false);
		}
		if (isFirst) {
			clickOnBar();
		}
	}

	void initComponents() {
		setContentView(R.layout.activity_capture);
		CaptureUtil.upgradeRootPermission("/dev/graphics/fb0");
		btnStartRecord = (Button) findViewById(R.id.buttonStart);
		btnStop = (Button) findViewById(R.id.buttonStop);
		btnStop.setEnabled(false);
		btnStop.setVisibility(View.GONE);
		pbar = (ProgressBar) findViewById(R.id.progressBar1);
		pbar.setVisibility(View.GONE);
		serviceIntent = new Intent(this, CaptureService.class);
		btnStartRecord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				makeNotify();
				FileWorker.deleteOldVideoFile();
				((MyApplication) getApplication()).isRecording = true;
				startService(serviceIntent);
				btnStop.setEnabled(true);
				btnStartRecord.setEnabled(false);
				finish();
			}
		});

		btnStop.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				stopAndConvert();
			}
		});
	}

	void clickOnBar() {
		Intent intent2 = getIntent();
		if (intent2 != null && intent2.getBooleanExtra("Stop", false)) {
			if (serviceIntent != null) {
				stopAndConvert();
			}
		}
	}

	void stopAndConvert() {
		stopService(serviceIntent);
		Toast toast = Toast.makeText(getApplicationContext(),
				"Recording Stoped", Toast.LENGTH_SHORT);
		toast.show();
		makeVideo();
		((MyApplication) getApplication()).isRecording = false;
		btnStop.setEnabled(false);
	}

	private void makeFail() {
		btnStartRecord.setEnabled(false);
		btnStop.setEnabled(false);
		pbar.setVisibility(View.GONE);
	}

	private void makeNotify() {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.stop)
				.setContentTitle("Video Capture")
				.setContentText("Press It To stop");
		Intent resultIntent = new Intent(this, CaptureActivity.class);
		resultIntent.putExtra("Stop", true);
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(CaptureActivity.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, mBuilder.build());
	}

	public void toast(String message) {
		Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
				0, 0);
		toast.show();
	}

	private void makeVideo() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} // for saving last pic
		FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());
		try {
			ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

				@Override
				public void onFailure() {
					Log.d(TAG, "Lib not loaded");
					makeFail();
				}

				@Override
				public void onSuccess() {
					Log.d(TAG, "Lib loaded");
				}
			});
		} catch (FFmpegNotSupportedException e) {
			e.printStackTrace();
		}
		try {
			pbar.setVisibility(ProgressBar.VISIBLE);
			String cmd = "-framerate 20 -start_number 10000 -i /sdcard/.Capture/Snapshot_%05d.jpg -vcodec mpeg4 -r 20 /sdcard/out3.mp4";
			Log.d(TAG, "Executing ffmpeg");
			ffmpeg.execute(cmd, new FFmpegStuff(getApplicationContext(), pbar));
		} catch (FFmpegCommandAlreadyRunningException e) {
			// Handle if FFmpeg is already running
		}
		//pbar.setVisibility(View.GONE);
		btnStartRecord.setEnabled(true);
	}
}
