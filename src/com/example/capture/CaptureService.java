package com.example.capture;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

public class CaptureService extends Service implements Runnable {
	private static final String TAG = "Service Log";
	private final int START_VALUE = 10000;
	private final int MAX_COUNT = START_VALUE + 6000; // ~ 5 mins
	private final String path = "/.Capture/Snapshot_";
	private final String extension = ".jpg";
	private final String sdcard = Environment.getExternalStorageDirectory().toString();
	private volatile boolean isRun = false;
	private int count = START_VALUE;
	private Thread thrd = new Thread(this);;
	
	@SuppressLint("NewApi")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "on start");
		isRun = true;
		thrd.start();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run() {
		Log.d(TAG, "Service started");
		while (count <= MAX_COUNT && isRun) {
			String fileName = sdcard + path + String.valueOf((count++)) + extension;
			CaptureUtil.capture(fileName);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
		Log.d(TAG, "Service stoped");
		stopSelf();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		isRun = false;
	}

}
