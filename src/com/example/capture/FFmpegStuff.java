package com.example.capture;


import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;

public class FFmpegStuff extends ExecuteBinaryResponseHandler {
	Context appContext;
	ProgressBar myBar;
	public FFmpegStuff(Context context, ProgressBar bar) {
		appContext = context;
		myBar =  bar;
		// TODO Auto-generated constructor stub
	}
	private static final String TAG = FFmpegStuff.class.getName();

	public void toast(String message) {
		Toast toast = Toast.makeText(appContext, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
				0, 0);
		toast.show();
	}

	@Override
	public void onStart() {
		Log.d(TAG, "Encoding");
		Toast toast = Toast.makeText(appContext, "Start encoding",
				Toast.LENGTH_SHORT);
		toast.show();
	}

	// @Override
	// public void onProgress(String message) {
	// }

	// @Override
	// public void onFailure(String message) {
	// toast("Failure");
	// throw new Exception("Failure");
	// Log.d(TAG, "FFmpeg error loaded");
	// Log.d(TAG, message);
	// }

	@Override
	public void onSuccess(String message) {
		toast("Video was created");
		Log.d(TAG, "Video created");
		FileWorker.cleanTempFiles();
		myBar.setVisibility(ProgressBar.GONE);
	}

	@Override
	public void onFinish() {
	}
}
