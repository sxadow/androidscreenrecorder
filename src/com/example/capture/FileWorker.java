package com.example.capture;

import java.io.DataOutputStream;
import java.io.IOException;

import android.util.Log;

public class FileWorker {
	private static final String TAG = FileWorker.class.getName();

	public static void deleteOldVideoFile() {
		DataOutputStream os = null;
		Process process;
		try {
			process = Runtime.getRuntime().exec("rm /sdcard/out3.mp4");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes("exit\n");
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void cleanTempFiles() {
		Process process = null;
		DataOutputStream os = null;
		try {
			Log.d(TAG, "deleting tmp dir");
			process = Runtime.getRuntime().exec("rm -rf /sdcard/.Capture");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes("exit\n");
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
