#ifndef THREADSTRUCT_H_
#define THREADSTRUCT_H_

#include <vector>
#include "MyArrayGuard.h"
typedef struct ThreadParam
{
	FBInfo *fb;
	MyArrayGuard<char> p_path;
	std::vector<unsigned char> *v_bits;
	~ThreadParam()
	{
		if(v_bits)
		{
			delete v_bits;
		}
		if(fb)
		{
			delete fb;

		}
	}
} Paramz;

#endif /* THREADSTRUCT_H_ */
