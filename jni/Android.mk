LOCAL_PATH := $(call my-dir)



#prebuilt libjpeg
include $(CLEAR_VARS)
LOCAL_MODULE := mjpeg
LOCAL_SRC_FILES := lib/libjpeg.a

include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := ARM_ARCH
LOCAL_SRC_FILES := armeabi-v7a/libARM_ARCH.so
include $(PREBUILT_SHARED_LIBRARY)

#include $(PREBUILT_STATIC_LIBRARY)

#prebuilt stl
#include $(CLEAR_VARS)
#LOCAL_MODULE := libstlport
#LOCAL_SRC_FILES := lib/libstlport_static.a
#LOCAL_STATIC_LIBRARIES +=  libstlport
#LOCAL_C_INCLUDES += $(LOCAL_PATH)/include/src/	
#include $(PREBUILT_STATIC_LIBRARY)

#built gsnap.c
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
				captureutil.cpp \
				gsnap.cpp \

LOCAL_C_INCLUDES += $(LOCAL_PATH)/include		
LOCAL_MODULE := capture
LOCAL_STATIC_LIBRARIES := mjpeg
LOCAL_DISABLE_FORMAT_STRING_CHECKS := true
LOCAL_LDLIBS :=-llog

#LOCAL_C_INCLUDES += external/stlport/stlport 
include $(BUILD_SHARED_LIBRARY)

include $(call all-makefiles-under,$(LOCAL_PATH))

include $(CLEAR_VARS)