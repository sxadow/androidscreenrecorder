#include <stdio.h>
#include <pthread.h>
#include "gsnap.h"
#include "ThreadStruct.h"
#include <vector>
#include <android/log.h>
#include "com_example_capture_CaptureUtil.h"
#define LOG "captureutil"



void* ThreadFunc(void *arg)
{
	if(arg)
	{
		__android_log_print(ANDROID_LOG_ERROR,LOG,"In thread");
		Paramz param = *(Paramz *)arg;
		__android_log_print(ANDROID_LOG_ERROR,LOG,param.p_path.get());
		__android_log_print(ANDROID_LOG_ERROR,LOG,"Starting codding");
		snap2jpg(param.p_path.get(),100,param.fb);
		__android_log_print(ANDROID_LOG_ERROR,LOG,"Deleting..");
	}
	else
	{
		__android_log_print(ANDROID_LOG_ERROR,LOG,"Bad params..");
	}
}

void capture(MyArrayGuard<char> filepath)
{
	__android_log_print(ANDROID_LOG_ERROR,LOG,filepath.get());
	FBInfo fb;
	Paramz *param = new Paramz();
	param->fb = new FBInfo();
	memset(param->fb,0x00,sizeof(param->fb));
	int fd;
	if ((fd = fb_open(param->fb, "/dev/graphics/fb0")) == 0)
	{
		__android_log_print(ANDROID_LOG_ERROR,LOG,"open framebuffer OK");

		param->v_bits= new std::vector<unsigned char>(fb_size(param->fb));

		__android_log_print(ANDROID_LOG_ERROR,LOG,"Copy");
		memcpy((void*)&(*param->v_bits)[0], (void*)param->fb->bits, fb_size(param->fb));


		__android_log_print(ANDROID_LOG_ERROR,LOG,"unMaping and closing fd");
		fb_close(param->fb);

		param->fb->bits=&((*param->v_bits)[0]);
		param->p_path = filepath;

		__android_log_print(ANDROID_LOG_ERROR,LOG,"Creating thread");
		pthread_attr_t attr;
		pthread_attr_init( &attr );
		pthread_t firstThread;
		pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_DETACHED );
		pthread_create( &firstThread, &attr, &ThreadFunc, (void *) param );
	}
	else
	{
		__android_log_print(ANDROID_LOG_ERROR,LOG,"open framebuffer failed,%d",fd);
	}
	__android_log_print(ANDROID_LOG_ERROR,LOG,"on end");
}
