#include <stdio.h>
#include <pthread.h>
#include <android/log.h>
#include "MyArrayGuard.h"
#include "capture.cpp"
#include "com_example_capture_CaptureUtil.h"
#define LOG "captureutil"



MyArrayGuard<char> jstringTostring(JNIEnv* env, jstring jstr) {
	__android_log_print(ANDROID_LOG_ERROR,LOG,"Converting java String");
	char* rtn = NULL;

	jclass clsstring = env->FindClass("java/lang/String");
	jstring strencode = env->NewStringUTF("utf-8");
	jmethodID mid = env->GetMethodID(clsstring, "getBytes",
			"(Ljava/lang/String;)[B");
	jbyteArray barr = (jbyteArray) env->CallObjectMethod(jstr, mid, strencode);
	jsize alen = env->GetArrayLength(barr);
	jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
	if (alen > 0) {
		rtn = new char[alen + 1];
		memcpy(rtn, ba, alen);
		rtn[alen] = 0;
	}
	MyArrayGuard<char> p_rtn(rtn);
	env->ReleaseByteArrayElements(barr, ba, 0);

	return p_rtn;
}

JNIEXPORT void JNICALL Java_com_example_capture_CaptureUtil_capture
(JNIEnv *env, jclass jthiz, jstring path)
{
	__android_log_print(ANDROID_LOG_ERROR,LOG,"main call");

	MyArrayGuard<char> p_filepath = jstringTostring(env,path);
	__android_log_print(ANDROID_LOG_ERROR,LOG,"Sending...");
	 capture(p_filepath); // I know what I do
}



