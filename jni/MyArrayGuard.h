/*
 * MyArrayGuard.h
 *
 *  Created on: 05 ���. 2015 �.
 *      Author: ������
 */

#ifndef MYARRAYGUARD_H_
#define MYARRAYGUARD_H_

template<typename  T>
class MyArrayGuard
{
public:
    MyArrayGuard(const MyArrayGuard& source) : m_ptr(source.obtain()) { }
	explicit
		MyArrayGuard(T* pointer = 0) : m_ptr(pointer) { };
	~MyArrayGuard();
    MyArrayGuard & operator=(const MyArrayGuard &source){ reset(source.obtain()); return *this; } // zeroize pointer in source and assigning source data instead of our data
    T& operator*() { return *m_ptr; }
    const T& operator*() const { return *m_ptr; }
	T* get() const;
	T* release();
	void reset(T* pointer);
	void reset();
private:
    T* obtain() const; // for constant references
	mutable T* m_ptr;
};


template< typename  T >
MyArrayGuard<T>::~MyArrayGuard()
{
	if (m_ptr)
	{
		delete[] m_ptr;
	}
}

template< typename  T >
T* MyArrayGuard<T>::get() const
{
	return m_ptr;
}

template< typename  T >
void MyArrayGuard<T>::reset(T* pointer)
{
	if ((m_ptr != pointer ))
	{
		delete[] m_ptr;
	}
	m_ptr = pointer;
}

template< typename  T >
void MyArrayGuard<T>::reset()
{
	T* pointer = 0;
	this.reset(pointer);
}

template< typename  T >
T* MyArrayGuard<T>::release()
{
	T* tmp = m_ptr;
	m_ptr = 0;
	return tmp;
}

template< typename  T >
T* MyArrayGuard<T>::obtain() const
{
    T* tmp = m_ptr;
    m_ptr = 0;
    return tmp;
}



#endif /* MYARRAYGUARD_H_ */
